package edu.bu.ec504.hw2p2;

import edu.bu.ec504.hw2p2.hashing.HashFamily;

/**
 * Implements a Bloom filter
 */
public class BloomFilter {
    // CONSTRUCTORS

    BloomFilter(HashFamily theHashFamily) {
        storedHF = theHashFamily;
        filter = new boolean[storedHF.getRange()]; // the filter itself
    }

    void insert(int element) {
        for (int ii = 0; ii< storedHF.getNumHashes(); ii++)
            filter[ storedHF.apply(ii, element) ] = true;
    }

    // METHODS
    boolean inFilter(int element) {
        for (int ii = 0; ii < storedHF.getNumHashes(); ii++)
            if (!filter[storedHF.apply(ii, element)])
                return false;

        return true;
    }

    // FIELDS

    /**
     * The Bloom filter array.
     */
    private final boolean[] filter;

    /**
     * A stored version of the {@link HashFamily} used by this Bloom Filter.
     */
    private final HashFamily storedHF;
}
