package edu.bu.ec504.hw2p2.hashing;

/**
 * You should update this class's methods.
 */
public class MyHashFamily extends HashFamily {

    /**
     * Creates your hash family.
     * @param numHashes The number of hashes in your hash family.
     * @param range The range of each hash in your hash family.
     */
    public MyHashFamily(int numHashes, int range) {
        super(numHashes, range);
    }

    /**
     * @inheritDoc
     */
    @Override
    public int apply(int index, int elem) {
        // TODO: CHange this method
        if (index<0 || index>getNumHashes())
            throw new IndexOutOfBoundsException();

        return 0;
    }

}
