package edu.bu.ec504.hw2p2.hashing;

/**
 * An example of a simple hash family.
 */
public class SimpleHashFamily extends HashFamily {

  /**
   * @inheritDoc
   */
  public SimpleHashFamily(int numHashes, int range) {
    super(numHashes, range);
  }

  /**
   * @inheritDoc
   */
  @Override
  public int apply(int index, int elem) {
    if (index<0 || index>getNumHashes())
      throw new IndexOutOfBoundsException();

    return elem%getRange();
  }
}
