package edu.bu.ec504.hw2p2;

import edu.bu.ec504.hw2p2.hashing.HashFamily;
import edu.bu.ec504.hw2p2.hashing.MyHashFamily;
import edu.bu.ec504.hw2p2.hashing.SimpleHashFamily;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Main {
  static int numTests=0;   // the number of tests performed so far
  static double sumFp=0.0; // a sum of the false positive values seen so far

  public static void main(String[] args) {
    // some simple tests of MyHashFamily
    final int NUMH = 3;    // the number of hashes in the family
    final int RANGE = 10;  // the range of each hash in the family
    final int NUMI = 5;    // the number of elements to insert into the Bloom Filter

    // ... create a Bloom filter based on MyHashFamily with NUMH hashes in the range 0..RANGE-1
    BloomFilter MyBF = new BloomFilter(new MyHashFamily(NUMH,RANGE));
    // ... create a Bloom filter based on a SimpleHashFamily with NUMH hashes in the range 0..RANGE-1
    BloomFilter SimpleBF = new BloomFilter(new SimpleHashFamily(NUMH, RANGE));

    // ... populate the Bloom filters
    for (int ii=0; ii<NUMI; ii++) {
      MyBF.insert(ii);
      SimpleBF.insert(ii);
    }

    // ... test the Bloom filters
    int myCount=0;
    int simpleCount=0;
    for (int ii=0; ii<20; ii++) {
      if (MyBF.inFilter(ii)) myCount++;
      if (SimpleBF.inFilter(ii)) simpleCount++;
    }

    System.out.println("Results:");
    System.out.println("* Actual number of elements inserted in the Bloom Filter: "+NUMI);
    System.out.println("* MyBF identified "+myCount+" elements.");
    System.out.println("* SimpleBF identified "+simpleCount+" elements.");
  }
}
